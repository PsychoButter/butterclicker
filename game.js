var multipler = 1;
var HMbutter = 0;
var multiplerclick = 1;
var autoclick = 0;
var farm = 0;
var knife = 0;
var factory = 0;
var alchemicfactory = 0;
var robot = 0;

function update() {
	document.getElementById('text').value = HMbutter;
	document.title = HMbutter + " butters";

	document.getElementById('HMautoclick').innerHTML = "   " + autoclick + " autoclickers";
	document.getElementById('costautoclick').innerHTML = ((autoclick+1) * 12) + " butters";

	document.getElementById('HMfarm').innerHTML = "   " + farm + " farms";
	document.getElementById('costfarm').innerHTML = ((farm+1) * 30) + " butters";

	document.getElementById('HMknife').innerHTML = "   " + knife + " knifes";
	document.getElementById('costknife').innerHTML = ((knife+1) * 1500) + " butters";

	document.getElementById('HMfactory').innerHTML = "   " + factory + " factorys";
	document.getElementById('costfactory').innerHTML = ((factory+1) * 16000) + " butters";

	document.getElementById('HMalchemicfactory').innerHTML = "   " + alchemicfactory + " alchemic factorys";
	document.getElementById('costalchemicfactory').innerHTML = ((alchemicfactory+1) * 180000) + " butters";

	document.getElementById('HMrobot').innerHTML = "   " + robot + " robots";
	document.getElementById('costrobot').innerHTML = ((robot+1) * 1000000) + " butters";

	document.getElementById('HMmultiplerclick').innerHTML = "   " + multiplerclick + " butters per click";
	document.getElementById('costmultiplerclick').innerHTML = ((multiplerclick+1)* 8) + " butters";

	document.getElementById('BPerSec').innerHTML = "Collect " + (((autoclick)+(farm*2)+(knife*10)+(factory*100)+(alchemicfactory*1000)+(robot*10000))*multipler) + " butters per second";
}

//timer
function timer() {
		HMbutter = HMbutter + autoclick;
		HMbutter = HMbutter + farm*2;
		HMbutter = HMbutter + knife*10;
		HMbutter = HMbutter + factory*100;
		HMbutter = HMbutter + alchemicfactory*1000;
		HMbutter = HMbutter + robot*10000;
		update()
}
setInterval(timer, 1000)
//add per click
function add() {
			HMbutter = HMbutter + multiplerclick;
			update()
		}
//save
function save() {
	localStorage.setItem("HMbutter", HMbutter);
	localStorage.setItem("autoclick", autoclick);
	localStorage.setItem("farm", farm);
	localStorage.setItem("knife", knife);
	localStorage.setItem("factory", factory);
	localStorage.setItem("alchemicfactory", alchemicfactory);
	localStorage.setItem("robot", robot);
	localStorage.setItem("multiplerclick", multiplerclick);
}

function load() {
	HMbutter = localStorage.getItem("HMbutter");
	HMbutter = parseInt(HMbutter);

	autoclick = localStorage.getItem("autoclick");
	autoclick = parseInt(autoclick);

	farm = localStorage.getItem("farm");
	farm = parseInt(farm);

	knife = localStorage.getItem("knife");
	knife = parseInt(knife);

	factory = localStorage.getItem("factory");
	factory = parseInt(factory);

	alchemicfactory = localStorage.getItem("alchemicfactory");
	alchemicfactory = parseInt(alchemicfactory);

	robot = localStorage.getItem("robot");
	robot = parseInt(robot);

	multiplerclick = localStorage.getItem("multiplerclick");
	multiplerclick = parseInt(multiplerclick);
	update()
}

function buyautoclick() {
	if(HMbutter >= ((autoclick+1) * 12)) {
		HMbutter = HMbutter - ((autoclick+1) * 12);
		autoclick = autoclick + 1;
		update()
	}
}

function buyfarm() {
	if(HMbutter >= ((farm+1) * 30)) {
		HMbutter = HMbutter - ((farm+1) * 30);
		farm = farm + 1;
		update()
}
}

function buyknife() {
	if(HMbutter >= ((knife+1) * 1500)) {
		HMbutter = HMbutter - ((knife+1) * 1500);
		knife = knife + 1;
		update()
	}
}

function buyfactory() {
	if(HMbutter >= ((factory+1) * 16000)) {
		HMbutter = HMbutter - ((factory+1) * 16000);
		factory = factory + 1;
		update()
	}
}

function buyalchemicfactory() {
	if(HMbutter >=((alchemicfactory+1) * 180000)) {
		HMbutter = HMbutter - ((alchemicfactory+1) * 180000);
		HMbutter = HMbutter + 1;
		update()
	}
}

function buyrobot() {
	if(HMbutter >=((r+1) * 1000000)) {
		HMbutter = HMbutter - ((robot+1) * 1000000);
		robot = robot + 1;
		update()
	}
}

function buymultiplerclick() {
	if(HMbutter >=((multiplerclick+1)*8)) {
		HMbutter = HMbutter - ((multiplerclick+1)*8);
		multiplerclick = multiplerclick + 1;
		update()
	}
}
